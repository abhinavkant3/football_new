

// '.tbl-content' consumed little space for vertical scrollbar, scrollbar width depend on browser/os/platfrom. Here calculate the scollbar width .
$(window).on("load resize ", function() {
  var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
  $('.tbl-header').css({'padding-right':scrollWidth});
}).resize();




$(document).ready(function(){
	$.ajax({
	    url: 'https://cors.io/?http://www.mocky.io/v2/5cd706cd3000002d4a606215',
	    type: 'GET',
	    contentType: "application/json; charset=utf-8",
	    success: function(data) { 
	    	resp = JSON.parse(data)
	    	if(resp && resp.standings && resp.standings.groups && resp.standings.groups[0] && resp.standings.groups[0].teams && resp.standings.groups[0].teams.team){
	    		var teamsArr = resp.standings.groups[0].teams.team;
	    		var standingsTbody =document.getElementById("tbody-standings");
	    		for(var i in teamsArr){
					// position
					var rank =  document.createElement("td");
					rank.innerHTML = teamsArr[i].position;
					// Team Name
					var teamName =  document.createElement("td");
					teamName.innerHTML = teamsArr[i].team_name;
					// Match Played
					var played =  document.createElement("td");
					played.innerHTML = teamsArr[i].played;
					//wins
					var wins =  document.createElement("td");
					wins.innerHTML = teamsArr[i].wins;
					//draws
					var draws =  document.createElement("td");
					draws.innerHTML = teamsArr[i].draws;
					//lost
					var lost =  document.createElement("td");
					lost.innerHTML = teamsArr[i].lost;
					//GF
					var goalsFor =  document.createElement("td");
					goalsFor.innerHTML = teamsArr[i].gf;
					//GA
					var goalsAgainst =  document.createElement("td");
					goalsAgainst.innerHTML = teamsArr[i].ga;
					//GD
					var goalDiff =  document.createElement("td");
					goalDiff.innerHTML = teamsArr[i].gf - teamsArr[i].ga;
					//PTS
					var points =  document.createElement("td");
					points.innerHTML = teamsArr[i].points;

					var tr = document.createElement("tr");
					tr.appendChild(rank);
					tr.appendChild(teamName);
					tr.appendChild(played);
					tr.appendChild(wins);
					tr.appendChild(draws);
					tr.appendChild(lost);
					tr.appendChild(goalsFor);
					tr.appendChild(goalsAgainst);
					tr.appendChild(goalDiff);
					tr.appendChild(points);
					standingsTbody.appendChild(tr);
				}
	    	}
	    	debugger;
	    }
	});
});